#include "MyForm.h"

/*
entries:Splitの分断結果
*road_shape:分断結果を格納するための構造体ポインタ
*/
void Show_result2(array<String^>^entries, ROAD_SHAPE *road_shape)
{
	Console::WriteLine("The return value contains these {0} elements:", entries->Length);
	System::Collections::IEnumerator^ myEnum = entries->GetEnumerator();
	int i = 0;
	while (myEnum->MoveNext())
	{
		String^ entry = safe_cast<String^>(myEnum->Current);
		Console::Write("<{0}>", entry);
		if (i == 0){ road_shape->distance = XmlConvert::ToDouble(entry->Trim()); i++; }
		else if (i == 1){ road_shape->height_of_right = XmlConvert::ToDouble(entry->Trim()); i++; }
		else if (i == 2){ road_shape->height_of_left = XmlConvert::ToDouble(entry->Trim()); i++; }
	}

	Console::Write("{0}{0}", Environment::NewLine);
}


//円と線分の衝突判定
int	 Collision_Detection(double circle_r,double circle_x, double circle_y, double x_s, double y_s, double x_e, double y_e){
	
	Vector_2D a, b, s;
	a.x = circle_x - x_s;
	a.y = circle_y - y_s;
	b.x = circle_x - x_e;
	b.y = circle_y - y_e;
	s.x = x_e - x_s;
	s.y = y_e - y_s;
	double result = abs(s.x*a.y - s.y*a.x) / sqrt(s.x*s.x + s.y*s.y);
	double result2 = (a.x*s.x + a.y*s.y)*(b.x*s.x + b.y*s.y);
	if (result > circle_r){  return 0; }
	if (result2 <= 0){return 1; }
	if (circle_r > sqrt(a.x*a.x + a.y*a.y) || circle_r > sqrt(b.x*b.x + b.y*b.y)){ return 1; }
	return 0;
	
}


/*
	車輪付リンクと付近の路面の接触を判定　前が接触してる：1　後ろが接触してる:2　してない：0
	which_wheel:左右どちらについての計算か　0:右 1:左
*/
int	 Collision_Detection_of_Link_with_Wheels(int which_wheel,int range, double radius_of_wheels, double length_bettween_wheels,ROAD_SHAPE *road_shape, PARAM_OF_LINK *param_of_link){
	
	Vector_2D wheel1, wheel2;
	int ditect;

	//-------車輪の座標を計算-----------
	//whel1:前　wheel2:後ろ
	if (which_wheel == 0){
		wheel1.x = road_shape->distance + length_bettween_wheels * cos(param_of_link->angle_of_right);
		wheel1.y = param_of_link->y_of_center_of_link_of_right_side + length_bettween_wheels * sin(param_of_link->angle_of_right);
		wheel2.x = road_shape->distance - length_bettween_wheels * cos(param_of_link->angle_of_right);
		wheel2.y = param_of_link->y_of_center_of_link_of_right_side - length_bettween_wheels * sin(param_of_link->angle_of_right);
	}
	else if (which_wheel == 1){
		wheel1.x = road_shape->distance + length_bettween_wheels * cos(param_of_link->angle_of_left);
		wheel1.y = param_of_link->y_of_center_of_link_of_left_side + length_bettween_wheels * sin(param_of_link->angle_of_left);
		wheel2.x = road_shape->distance - length_bettween_wheels * cos(param_of_link->angle_of_left);
		wheel2.y = param_of_link->y_of_center_of_link_of_left_side - length_bettween_wheels * sin(param_of_link->angle_of_left);
	}
	//------------------------------------

	//--------前後の車輪と付近の地形の直線が接しているか---------------
	road_shape = road_shape - range;
	for (int i = 0; i < 2 * range; i++){
		if (which_wheel == 0){
			ditect = Collision_Detection(radius_of_wheels, wheel1.x, wheel1.y, (road_shape + i)->distance, (road_shape + i)->height_of_right, (road_shape + i + 1)->distance, (road_shape + i + 1)->height_of_right);
			if (ditect == 1){ return 1; }
			if (ditect == 0){ ditect = Collision_Detection(radius_of_wheels, wheel2.x, wheel2.y, (road_shape + i)->distance, (road_shape + i)->height_of_right, (road_shape + i + 1)->distance, (road_shape + i + 1)->height_of_right); }
			if (ditect == 1){ return 2; }
		}
		else if (which_wheel == 1){
			ditect = Collision_Detection(radius_of_wheels, wheel1.x, wheel1.y, (road_shape + i)->distance, (road_shape + i)->height_of_left, (road_shape + i + 1)->distance, (road_shape + i + 1)->height_of_left);
			if (ditect == 1){ return 1; }
			if (ditect == 0){ ditect = Collision_Detection(radius_of_wheels, wheel2.x, wheel2.y, (road_shape + i)->distance, (road_shape + i)->height_of_left, (road_shape + i + 1)->distance, (road_shape + i + 1)->height_of_left); }
			if (ditect == 1){ return 2; }
		}
	}
	//-----------------------------------------------------------------------
	return 0;
}


void Set_param_of_link(PARAM_OF_LINK *pol,double set_height, double set_angle){
	pol->angle_of_left = set_angle;
	pol->angle_of_right = set_angle;
	pol->y_of_center_of_link_of_left_side = set_height;
	pol->y_of_center_of_link_of_right_side = set_height;
}

