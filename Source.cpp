#include "MyForm.h"

using namespace Wheel_Path_Making;

[STAThreadAttribute]
int main(){
	MyForm ^fm = gcnew MyForm();
	fm->ShowDialog();
	return 0;
}