//路面形状を表す構造体
struct ROAD_SHAPE{
	double distance;
	double height_of_right;
	double height_of_left;

};

//車体の位置とリンクの角度の関係を表す構造体
//精度は整数値程度
struct PARAM_OF_LINK{
	double y_of_center_of_link_of_right_side;
	double y_of_center_of_link_of_left_side;
	double angle_of_right;
	double angle_of_left;
};

//二次元ベクトル
struct Vector_2D{
	double x;
	double y;
};