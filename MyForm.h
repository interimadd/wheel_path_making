#pragma once

/*
#include <iostream>
#include <string>
#include <list>
#include <vector>
/*
#include <boost/algorithm/string.hpp>
*/
#include <Windows.h>
#include "Original_Structs.h"
#include <math.h>
using namespace System::IO;
using namespace System::Xml;
using namespace std;
using namespace System;


void Show_result2(array<String^>^entries,ROAD_SHAPE *road_shape);
int	 Collision_Detection(double circle_r,double circle_x, double circle_y, double x_s, double y_s, double x_e, double y_e);
int	 Collision_Detection_of_Link_with_Wheels(int which_wheel,int range,double radius_of_wheels, double length_bettween_wheels, ROAD_SHAPE *road_shape, PARAM_OF_LINK *param_of_link);
void Set_param_of_link(PARAM_OF_LINK *pol,double set_height,double set_angle);

namespace Wheel_Path_Making {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class MyForm : public System::Windows::Forms::Form
	{

	private:ROAD_SHAPE *road_shape;
	private:PARAM_OF_LINK *param_of_link;
	private:double *slider_x;
	private:int num_of_param;

	private:double length_between_wheels;//車輪間リンク中心から車輪の距離
	private:double radius_of_wheel;
	private:double body_height;
	private:double body_width;

	private:int now_point;

	//----------------描画に必要なポインタなど。他プログラム参照----------------------
	private:Graphics^ gr;		//描画用ポインタ1
	private:Graphics^ gr2;		//描画用ポインタ2
	private:Pen^ blackPen;		//描線用ポインタ（黒）
	private:Pen^ whitePen;		//描線用ポインタ（白）
	private:SolidBrush^ redBrush;	    //塗潰し用ポインタ（赤）
	private:SolidBrush^ blackBrush;	    //塗潰し用ポインタ（黒）
	private:SolidBrush^ blueBrush;	    //塗潰し用ポインタ（青）
	private:SolidBrush^ greenBrush;	    //塗潰し用ポインタ（緑）
	private:SolidBrush^ yellowBrush;	//塗潰し用ポインタ（黄）
	private:SolidBrush^ brawnBrush;	    //塗潰し用ポインタ（茶）
	private:array<SolidBrush^>^ atomBrush;	        //各原子の色
	private:BufferedGraphicsContext^ myContext;		//画像バッファ
	private:BufferedGraphicsContext^ myContext2;		//画像バッファ
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private:BufferedGraphics^ myBuffer;				//画像バッファ
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;

	private:BufferedGraphics^ myBuffer2;				//画像バッファ
	//----------------------------------------------------------------------------------

	public:
		MyForm(void)
		{
			//ここでパラメータなど設定
			InitializeComponent();
			//
			//TODO: ここにコンストラクター コードを追加します

			//------------------各種パラメータ設定-------------------------------
			num_of_param = 1000;																	//波状路の点の数1000
			road_shape = new ROAD_SHAPE[num_of_param];
			param_of_link = new PARAM_OF_LINK[num_of_param];
			slider_x = new double[num_of_param];

			length_between_wheels = 130;															//車輪からリンク中心までの距離130mm
			radius_of_wheel = 120;																	//車輪半径120mm
			body_height = 400;
			body_width = 300;
			now_point = 0;
			//--------------------------------------------------------------------

			//-------------------描写用-------------------------------------------
			redBrush = gcnew SolidBrush(Color::Red);		//塗潰し（赤）
			blackBrush = gcnew SolidBrush(Color::Black);	//塗潰し（黒）
			blueBrush = gcnew SolidBrush(Color::Blue);	//塗潰し（青）
			greenBrush = gcnew SolidBrush(Color::Green);	//塗潰し（青）
			yellowBrush = gcnew SolidBrush(Color::Yellow);	//塗潰し（黄）
			brawnBrush = gcnew SolidBrush(Color::SaddleBrown);	//塗潰し（黄）
			blackPen = gcnew Pen(System::Drawing::Color::Black, 3);	//描線（黒）
			whitePen = gcnew Pen(System::Drawing::Color::White, 6);	//描線（黒）
			gr = pictureBox1->CreateGraphics();						//描画
			gr2 = pictureBox2->CreateGraphics();
			//---------------------------------------------------------------------
		}

	protected:
		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  FileReadButton;
	protected:

	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog2;
	private: System::Windows::Forms::Button^  ShowPath;
	private: System::Windows::Forms::Label^  label1;

	private: System::ComponentModel::IContainer^  components;
	protected:

	private:
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->FileReadButton = (gcnew System::Windows::Forms::Button());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->openFileDialog2 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->ShowPath = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			this->SuspendLayout();
			// 
			// FileReadButton
			// 
			this->FileReadButton->Location = System::Drawing::Point(1189, 559);
			this->FileReadButton->Name = L"FileReadButton";
			this->FileReadButton->Size = System::Drawing::Size(110, 53);
			this->FileReadButton->TabIndex = 0;
			this->FileReadButton->Text = L"FileRead";
			this->FileReadButton->UseVisualStyleBackColor = true;
			this->FileReadButton->Click += gcnew System::EventHandler(this, &MyForm::FileReadButton_Click);
			// 
			// pictureBox1
			// 
			this->pictureBox1->BackColor = System::Drawing::SystemColors::Window;
			this->pictureBox1->Location = System::Drawing::Point(12, 24);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(865, 588);
			this->pictureBox1->TabIndex = 1;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->Click += gcnew System::EventHandler(this, &MyForm::pictureBox1_Click);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			// 
			// openFileDialog2
			// 
			this->openFileDialog2->FileName = L"openFileDialog2";
			this->openFileDialog2->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &MyForm::openFileDialog2_FileOk);
			// 
			// ShowPath
			// 
			this->ShowPath->Location = System::Drawing::Point(1189, 497);
			this->ShowPath->Name = L"ShowPath";
			this->ShowPath->Size = System::Drawing::Size(110, 54);
			this->ShowPath->TabIndex = 2;
			this->ShowPath->Text = L"ShowPath";
			this->ShowPath->UseVisualStyleBackColor = true;
			this->ShowPath->Click += gcnew System::EventHandler(this, &MyForm::ShowPath_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(1189, 464);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(35, 12);
			this->label1->TabIndex = 3;
			this->label1->Text = L"label1";
			// 
			// pictureBox2
			// 
			this->pictureBox2->BackColor = System::Drawing::SystemColors::Window;
			this->pictureBox2->Location = System::Drawing::Point(902, 24);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(397, 401);
			this->pictureBox2->TabIndex = 4;
			this->pictureBox2->TabStop = false;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(1083, 562);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(47, 50);
			this->button1->TabIndex = 5;
			this->button1->Text = L">";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(1027, 562);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(50, 50);
			this->button2->TabIndex = 6;
			this->button2->Text = L"<";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 12);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1311, 626);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->pictureBox2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->ShowPath);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->FileReadButton);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	//路面形状ファイル読み込みボタン
	private: System::Void FileReadButton_Click(System::Object^  sender, System::EventArgs^  e) {
				 

		//-------------------路面形状ファイル読み込み------------------------------------
				 openFileDialog1->FileName = "*.txt";															//txt形式の読み込み

				 if (openFileDialog1->ShowDialog() != System::Windows::Forms::DialogResult::OK){
					 label1->Text = "openFile is canceled";
					 return;
				 }

				FileStream ^fs = gcnew FileStream(openFileDialog1->FileName, FileMode::Open);
				StreamReader ^ fin = gcnew StreamReader(fs, System::Text::Encoding::GetEncoding("UTF-8"));
				String ^s;																						//System::Stringとかいうやつ。stringやcharのように扱えずに困った

				for (int i = 0; i < num_of_param; i++){
					s = fin->ReadLine();																		//一行ずつ読み込み
					array<Char>^sep = gcnew array<Char>{'	'};													//どの文字でsplitするか。今回はtab
					array<String^>^result;																		//Split結果を格納する配列。よくわかっていない
			        result = s->Split(sep, StringSplitOptions::None);
					Show_result2(result,road_shape+i);															//路面形状をコンソールに表示とroad_shape配列に格納
					//Console::WriteLine("distance {0}", (road_shape + i)->distance);
				}
				
				fin->Close();  //ファイルのクローズ
		//---------------------路面形状ファイル読み込み終了----------------------------------

		//---------------------左右それぞれリンク角度計算----------------------------------
				for (int i = 0; i < num_of_param; i++){
					if (road_shape->distance < radius_of_wheel + length_between_wheels){ Set_param_of_link(param_of_link, -1,-1); }
					else if ((road_shape - i + num_of_param - 1)->distance - road_shape->distance < radius_of_wheel + length_between_wheels){ Set_param_of_link(param_of_link, -1,-1); }		//車輪が外に出てしまう条件では無視
					else{
						
						int range;																				 //現在位置のroad_shapeから＋−いくつまで考えるか
						for (range = 0; (road_shape + range)->distance - road_shape->distance < radius_of_wheel + length_between_wheels; range++);
						double height_of_link = max(road_shape->height_of_left , road_shape->height_of_right) + 2*radius_of_wheel;
						Set_param_of_link(param_of_link, height_of_link, 0);

						
						//上から車輪付きリンクを下ろしていって→前の車輪が当たれば左回転→さらに下ろすをroop回
						int roop_ = 200;
						for (int j = 0; j < roop_; j++){
							int ditect = Collision_Detection_of_Link_with_Wheels(0, range, radius_of_wheel, length_between_wheels, road_shape, param_of_link);
							if (ditect == 0){ param_of_link->y_of_center_of_link_of_right_side -= 1; }
							else if (ditect == 1){ param_of_link->angle_of_right += 0.02; }
							else if (ditect == 2){ param_of_link->angle_of_right -= 0.02; }
						}
						for (int j = 0; j < roop_; j++){
							int ditect = Collision_Detection_of_Link_with_Wheels(1, range, radius_of_wheel, length_between_wheels, road_shape, param_of_link);
							if (ditect == 0){ param_of_link->y_of_center_of_link_of_left_side -= 1; }
							else if (ditect == 1){ param_of_link->angle_of_left += 0.02; }
							else if (ditect == 2){ param_of_link->angle_of_left -= 0.02; }
						}

						//左右で車輪間リンク中心の低いほうが一輪接地なので、まず高いほうに合わせてから一輪接地できるようにあわせる
						if (param_of_link->y_of_center_of_link_of_right_side > param_of_link->y_of_center_of_link_of_left_side){
							param_of_link->y_of_center_of_link_of_left_side = param_of_link->y_of_center_of_link_of_right_side;
							while (Collision_Detection_of_Link_with_Wheels(1, range, radius_of_wheel, length_between_wheels, road_shape, param_of_link) == 0){
								param_of_link->angle_of_left += 0.02;
							}
						}
						else{
							param_of_link->y_of_center_of_link_of_right_side = param_of_link->y_of_center_of_link_of_left_side;
							while (Collision_Detection_of_Link_with_Wheels(0, range, radius_of_wheel, length_between_wheels, road_shape, param_of_link) == 0){
								param_of_link->angle_of_right += 0.02;
							}
						}

					}
					//Console::WriteLine("number {0}, height_of_right {1}, left {2}", i,param_of_link->y_of_center_of_link_of_right_side,param_of_link->y_of_center_of_link_of_left_side);
					Console::WriteLine("number {0}, ang_right {1}, ang_left {2}", i, param_of_link->angle_of_right, param_of_link->angle_of_left);
					road_shape++;
					param_of_link++;
				}
				road_shape -= num_of_param;
				param_of_link -= num_of_param;
				Console::WriteLine("Finsih Reading and Calculating");

		//---------------------左右それぞれリンク角度計算終了----------------------------------
				
		//---------------------タイヤの接地状況を調べる。タイヤの中心から+-11°のところで接地できているかどうか------
		//---------------------追加でスライダの位置計算--------------------------------------------------------------
				double enable_l = radius_of_wheel * sin(11.0*3.14/180.0);        //+-11°
				for (int i = 0; i < num_of_param; i++){
					slider_x[i] = 10000; //スライダ位置初期化（適当）
					if ((param_of_link+i)->angle_of_left != -1){

						//念のためちょっと下げる
						const double down = 5;
						(param_of_link+i)->y_of_center_of_link_of_left_side -= down;
						(param_of_link+i)->y_of_center_of_link_of_right_side -= down;

						//左右前後のタイヤについて1mm下げたときに接地しているかをチェック
						int un_ground[2][2] = { 0 };

						int k_min = 0;
						int k_max = 0;

						//右前のタイヤについて
						while (1){
							//Console::WriteLine("i:{0},length:{1},ang:{2},enable:{3}",i, length_between_wheels,(param_of_link + i)->angle_of_right , enable_l);
							if (length_between_wheels*cos((param_of_link + i)->angle_of_right) - enable_l < (road_shape + i + k_min)->distance - (road_shape + i)->distance){ break; }
							k_min++;
						}
						k_max = k_min+1;
						while (1){
							if (length_between_wheels*cos((param_of_link + i)->angle_of_right) + enable_l < (road_shape + i + k_max)->distance - (road_shape + i)->distance){ break; }
							k_max++;
						}
						for (int k = k_min; k<k_max-1; k++){
							int ditect = Collision_Detection(radius_of_wheel, (road_shape + i)->distance + length_between_wheels*cos((param_of_link + i)->angle_of_right), 
								(param_of_link + i)->y_of_center_of_link_of_right_side + length_between_wheels*sin((param_of_link + i)->angle_of_right), 
								(road_shape + i + k)->distance, (road_shape + i + k)->height_of_right, (road_shape + i +k + 1)->distance, (road_shape + i + k + 1)->height_of_right);
							if (ditect){
								un_ground[0][0] = 1; break;
							}
						}
						//Console::WriteLine("min:{0},max:{1}", k_min, k_max);

						//右後ろのタイヤについて
						k_min = 0;
						k_max = 0;

						while (1){
							if (-length_between_wheels*cos((param_of_link + i)->angle_of_right) + enable_l > (road_shape + i - k_min)->distance - (road_shape + i)->distance){ break; }
							k_min++;
						}
						k_max = k_min + 1;
						while (1){
							if (-length_between_wheels*cos((param_of_link + i)->angle_of_right) - enable_l > (road_shape + i - k_max)->distance - (road_shape + i)->distance){ break; }
							k_max++;
						}
						for (int k = k_min; k<k_max - 1; k++){
							int ditect = Collision_Detection(radius_of_wheel, (road_shape + i)->distance - length_between_wheels*cos((param_of_link + i)->angle_of_right),
								(param_of_link + i)->y_of_center_of_link_of_right_side - length_between_wheels*sin((param_of_link + i)->angle_of_right),
								(road_shape + i - k)->distance, (road_shape + i - k)->height_of_right, (road_shape + i - k - 1)->distance, (road_shape + i - k - 1)->height_of_right);
							if (ditect){
								un_ground[0][1] = 1; break;
							}
						}

						//左前のタイヤについて
						k_min = 0;
						k_max = 0;
						while (1){
							if (length_between_wheels*cos((param_of_link + i)->angle_of_left) - enable_l < (road_shape + i + k_min)->distance - (road_shape + i)->distance){ break; }
							k_min++;
						}
						k_max = k_min + 1;
						while (1){
							if (length_between_wheels*cos((param_of_link + i)->angle_of_left) + enable_l < (road_shape + i + k_max)->distance - (road_shape + i)->distance){ break; }
							k_max++;
						}
						for (int k = k_min; k<k_max - 1; k++){
							int ditect = Collision_Detection(radius_of_wheel, (road_shape + i)->distance + length_between_wheels*cos((param_of_link + i)->angle_of_left),
								(param_of_link + i)->y_of_center_of_link_of_left_side + length_between_wheels*sin((param_of_link + i)->angle_of_left),
								(road_shape + i + k)->distance, (road_shape + i + k)->height_of_left, (road_shape + i + k + 1)->distance, (road_shape + i + k + 1)->height_of_left);
							if (ditect){
								un_ground[1][0] = 1; break;
							}
						}

						//左後ろのタイヤについて
						k_min = 0;
						k_max = 0;
						while (1){
							if (-length_between_wheels*cos((param_of_link + i)->angle_of_left) + enable_l > (road_shape + i - k_min)->distance - (road_shape + i)->distance){ break; }
							k_min++;
						}
						k_max = k_min + 1;
						while (1){
							if (-length_between_wheels*cos((param_of_link + i)->angle_of_left) - enable_l > (road_shape + i - k_max)->distance - (road_shape + i)->distance){ break; }
							k_max++;
						}
						for (int k = k_min; k<k_max - 1; k++){
							int ditect = Collision_Detection(radius_of_wheel, (road_shape + i)->distance - length_between_wheels*cos((param_of_link + i)->angle_of_left),
								(param_of_link + i)->y_of_center_of_link_of_left_side - length_between_wheels*sin((param_of_link + i)->angle_of_left),
								(road_shape + i - k)->distance, (road_shape + i - k)->height_of_left, (road_shape + i - k - 1)->distance, (road_shape + i - k - 1)->height_of_left);
							if (ditect){
								un_ground[1][1] = 1; break;
							}
						}

						(param_of_link + i)->y_of_center_of_link_of_left_side += down;
						(param_of_link + i)->y_of_center_of_link_of_right_side += down;
						//三点でちゃんと着地できていなければ計算結果を取り消す
						//Console::WriteLine("{0},{1},{2},{3}", un_ground[0][0] , un_ground[0][1] , un_ground[1][0] , un_ground[1][1]);
						if (un_ground[0][0] + un_ground[0][1] + un_ground[1][0] + un_ground[1][1] < 3){ Set_param_of_link((param_of_link+i), -1, -1); }
						else{
							if (un_ground[0][0] == 0){ slider_x[i] = -length_between_wheels*cos((param_of_link + i)->angle_of_right)/2.0; }
							else if (un_ground[0][1] == 0){ slider_x[i] = length_between_wheels*cos((param_of_link + i)->angle_of_right)/2.0; }
							else if (un_ground[1][0] == 0){ slider_x[i] = -length_between_wheels*cos((param_of_link + i)->angle_of_left) / 2.0; }
							else if (un_ground[0][1] == 0){ slider_x[i] = length_between_wheels*cos((param_of_link + i)->angle_of_left) / 2.0; }
							else{ slider_x[i] = slider_x[i-1]; }//四点接地している場合（前と同じにする）
						}

					}
					//Console::WriteLine("num:{0},slider_x:{1}", i, slider_x[i]); Sleep(20);
				}
				Console::WriteLine("Finsih checking ground state");
	}

private: System::Void ShowPath_Click(System::Object^  sender, System::EventArgs^  e) {
			
			 myContext = gcnew BufferedGraphicsContext();
			 myBuffer = myContext->Allocate(pictureBox1->CreateGraphics(), pictureBox1->DisplayRectangle);
			 myBuffer->Graphics->Clear(Color::White);

			 myContext2 = gcnew BufferedGraphicsContext();
			 myBuffer2 = myContext2->Allocate(pictureBox2->CreateGraphics(), pictureBox2->DisplayRectangle);
			 myBuffer2->Graphics->Clear(Color::White);

			 int camx = pictureBox1->Width;		//pictureboxの幅
			 int camy = pictureBox1->Height;		//pictureboxの高さ
			 double scale = 5.0;							//表示倍率5→1/5で表示
			 int radius_of_wheels_for_render	  = int(radius_of_wheel / (double)scale);
			 int length_between_wheels_for_render = int(length_between_wheels / (double)scale);

			 int center_of_pic2_x = pictureBox2->Width/2;
			 int center_of_pic2_y = pictureBox2->Height/2;
			 double scale2 = 3.0;

			 for (int j=0; j < num_of_param; j++){
				 if ((param_of_link + j)->angle_of_left != -1){
					 myBuffer->Graphics->Clear(Color::White);
					 int lc_r = int(length_between_wheels_for_render * cos((param_of_link + j)->angle_of_right));
					 int ls_r = int(length_between_wheels_for_render * sin((param_of_link + j)->angle_of_right));
					 int lc_l = int(length_between_wheels_for_render * cos((param_of_link + j)->angle_of_left));
					 int ls_l = int(length_between_wheels_for_render * sin((param_of_link + j)->angle_of_left));
					 int center_of_link_x = int((road_shape + j)->distance / scale) % camx;
					 int step_x = int((road_shape + j)->distance / scale) / camx;
					 int center_of_link_r = int(10 + 300 * step_x + (param_of_link + j)->y_of_center_of_link_of_right_side / scale);
					 int center_of_link_l = int(10 + 300 * step_x + (param_of_link + j)->y_of_center_of_link_of_left_side / scale);
					 myBuffer->Graphics->DrawLine(blackPen, center_of_link_x - lc_r, center_of_link_r - ls_r, center_of_link_x + lc_r, center_of_link_r + ls_r);
					 myBuffer->Graphics->FillEllipse(redBrush, center_of_link_x - lc_r - radius_of_wheels_for_render, center_of_link_r - ls_r - radius_of_wheels_for_render, radius_of_wheels_for_render * 2, radius_of_wheels_for_render * 2);
					 myBuffer->Graphics->FillEllipse(redBrush, center_of_link_x + lc_r - radius_of_wheels_for_render, center_of_link_r + ls_r - radius_of_wheels_for_render, radius_of_wheels_for_render * 2, radius_of_wheels_for_render * 2);
					 myBuffer->Graphics->DrawLine(blackPen, center_of_link_x - lc_l, center_of_link_l - ls_l, center_of_link_x + lc_l, center_of_link_l + ls_l);
					 myBuffer->Graphics->FillEllipse(greenBrush, center_of_link_x - lc_l - radius_of_wheels_for_render, center_of_link_l - ls_l - radius_of_wheels_for_render, radius_of_wheels_for_render * 2, radius_of_wheels_for_render * 2);
					 myBuffer->Graphics->FillEllipse(greenBrush, center_of_link_x + lc_l - radius_of_wheels_for_render, center_of_link_l + ls_l - radius_of_wheels_for_render, radius_of_wheels_for_render * 2, radius_of_wheels_for_render * 2);
					 //スライダ描写
					 int slide_center_x = center_of_link_x + int(slider_x[j]*4.0/(3.0*scale));
					 int slide_center_y = center_of_link_r + int(body_height / scale);
					 myBuffer->Graphics->FillRectangle(brawnBrush, slide_center_x - 30, slide_center_y, 60, 40);

					 for (int i=0; i < num_of_param; i++){
						 int road_x = int((road_shape + i)->distance / scale) % camx;
						 int step = int((road_shape + i)->distance / scale) / camx;
						 int road_hl = int((road_shape + i)->height_of_left / scale);
						 int road_hr = int((road_shape + i)->height_of_right / scale);
						 myBuffer->Graphics->FillEllipse(greenBrush, road_x, 10 + road_hl + 300 * step, 2, 2);
						 myBuffer->Graphics->FillEllipse(redBrush, road_x, 10 + road_hr + 300 * step, 2, 2);						 
					 }
					 myBuffer->Render(gr);

					 myBuffer2->Graphics->Clear(Color::White);
					 myBuffer2->Graphics->FillRectangle(brawnBrush, center_of_pic2_x + int(-body_width*3.0 / (8.0*scale2)), center_of_pic2_y + int((slider_x[j]*4.0/3.0 - 150.0) / scale2), int(body_width*3.0 / (4.0*scale2)), int(300.0 / scale2));
					 myBuffer2->Graphics->DrawLine(blackPen, center_of_pic2_x + int(body_width / (2.0*scale2)), center_of_pic2_y + length_between_wheels*cos((param_of_link + j)->angle_of_right) / scale, center_of_pic2_x + int(body_width / (2.0*scale2)), center_of_pic2_y - length_between_wheels*cos((param_of_link + j)->angle_of_right) / scale);
					 myBuffer2->Graphics->DrawLine(blackPen, center_of_pic2_x - int(body_width / (2.0*scale2)), center_of_pic2_y + length_between_wheels*cos((param_of_link + j)->angle_of_left) / scale, center_of_pic2_x - int(body_width / (2.0*scale2)), center_of_pic2_y - length_between_wheels*cos((param_of_link + j)->angle_of_left) / scale);
					 myBuffer2->Graphics->FillRectangle(redBrush, center_of_pic2_x + int(body_width / (2.0*scale2)), center_of_pic2_y + length_between_wheels*cos((param_of_link + j)->angle_of_right) / scale - int(radius_of_wheel / scale), 20, int(radius_of_wheel*2.0 / scale));
					 myBuffer2->Graphics->FillRectangle(redBrush, center_of_pic2_x + int(body_width / (2.0*scale2)), center_of_pic2_y - length_between_wheels*cos((param_of_link + j)->angle_of_right) / scale - int(radius_of_wheel / scale), 20, int(radius_of_wheel*2.0 / scale));
					 myBuffer2->Graphics->FillRectangle(greenBrush, center_of_pic2_x - int(body_width / (2.0*scale2)) - 20, center_of_pic2_y + length_between_wheels*cos((param_of_link + j)->angle_of_left) / scale - int(radius_of_wheel / scale), 20, int(radius_of_wheel*2.0 / scale));
					 myBuffer2->Graphics->FillRectangle(greenBrush, center_of_pic2_x - int(body_width / (2.0*scale2)) - 20, center_of_pic2_y - length_between_wheels*cos((param_of_link + j)->angle_of_left) / scale - int(radius_of_wheel / scale), 20, int(radius_of_wheel*2.0 / scale));
					 myBuffer2->Graphics->FillRectangle(blackBrush, center_of_pic2_x - 3, center_of_pic2_y + int((slider_x[j] -3) /scale2), 6,6);
					 myBuffer2->Render(gr2);

					 Sleep(50);
				 }
				 else{ Sleep(100); }
			 }
			 
			

}

private: System::Void openFileDialog2_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {}
private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {}
private: System::Void pictureBox1_Click(System::Object^  sender, System::EventArgs^  e) {}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			 myContext = gcnew BufferedGraphicsContext();
			 myBuffer = myContext->Allocate(pictureBox1->CreateGraphics(), pictureBox1->DisplayRectangle);
			 myBuffer->Graphics->Clear(Color::White);

			 int camx = pictureBox1->Width;		//pictureboxの幅
			 int camy = pictureBox1->Height;		//pictureboxの高さ
			 double scale = 5.0;							//表示倍率5→1/5で表示
			 int radius_of_wheels_for_render = int(radius_of_wheel / (double)scale);
			 int length_between_wheels_for_render = int(length_between_wheels / (double)scale);

			 if ((param_of_link + now_point + 1)->angle_of_left == -1){
				 now_point++;
				 while ((param_of_link + now_point)->angle_of_left == -1){ now_point++; if (now_point >= num_of_param)break; }
			 }
			 else{
				 now_point++;
				 while ((param_of_link + now_point)->angle_of_left != -1){ now_point++; if (now_point >= num_of_param)break; }
				 now_point--;
			 }

			 myBuffer->Graphics->Clear(Color::White);
			 int j = now_point;
			 int lc_r = int(length_between_wheels_for_render * cos((param_of_link + j)->angle_of_right));
			 int ls_r = int(length_between_wheels_for_render * sin((param_of_link + j)->angle_of_right));
			 int lc_l = int(length_between_wheels_for_render * cos((param_of_link + j)->angle_of_left));
			 int ls_l = int(length_between_wheels_for_render * sin((param_of_link + j)->angle_of_left));
			 int center_of_link_x = int((road_shape + j)->distance / scale) % camx;
			 int step_x = int((road_shape + j)->distance / scale) / camx;
			 int center_of_link_r = int(10 + 300 * step_x + (param_of_link + j)->y_of_center_of_link_of_right_side / scale);
			 int center_of_link_l = int(10 + 300 * step_x + (param_of_link + j)->y_of_center_of_link_of_left_side / scale);
			 myBuffer->Graphics->DrawLine(blackPen, center_of_link_x - lc_r, center_of_link_r - ls_r, center_of_link_x + lc_r, center_of_link_r + ls_r);
			 myBuffer->Graphics->FillEllipse(redBrush, center_of_link_x - lc_r - radius_of_wheels_for_render, center_of_link_r - ls_r - radius_of_wheels_for_render, radius_of_wheels_for_render * 2, radius_of_wheels_for_render * 2);
			 myBuffer->Graphics->FillEllipse(redBrush, center_of_link_x + lc_r - radius_of_wheels_for_render, center_of_link_r + ls_r - radius_of_wheels_for_render, radius_of_wheels_for_render * 2, radius_of_wheels_for_render * 2);
			 myBuffer->Graphics->DrawLine(blackPen, center_of_link_x - lc_l, center_of_link_l - ls_l, center_of_link_x + lc_l, center_of_link_l + ls_l);
			 myBuffer->Graphics->FillEllipse(greenBrush, center_of_link_x - lc_l - radius_of_wheels_for_render, center_of_link_l - ls_l - radius_of_wheels_for_render, radius_of_wheels_for_render * 2, radius_of_wheels_for_render * 2);
			 myBuffer->Graphics->FillEllipse(greenBrush, center_of_link_x + lc_l - radius_of_wheels_for_render, center_of_link_l + ls_l - radius_of_wheels_for_render, radius_of_wheels_for_render * 2, radius_of_wheels_for_render * 2);

			 for (int i = 0; i < num_of_param; i++){
				 int road_x = int((road_shape + i)->distance / scale) % camx;
				 int step = int((road_shape + i)->distance / scale) / camx;
				 int road_hl = int((road_shape + i)->height_of_left / scale);
				 int road_hr = int((road_shape + i)->height_of_right / scale);
				 myBuffer->Graphics->FillEllipse(greenBrush, road_x, 10 + road_hl + 300 * step, 2, 2);
				 myBuffer->Graphics->FillEllipse(redBrush, road_x, 10 + road_hr + 300 * step, 2, 2);
			 }
			 myBuffer->Render(gr);
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
			 myContext = gcnew BufferedGraphicsContext();
			 myBuffer = myContext->Allocate(pictureBox1->CreateGraphics(), pictureBox1->DisplayRectangle);
			 myBuffer->Graphics->Clear(Color::White);

			 int camx = pictureBox1->Width;		//pictureboxの幅
			 int camy = pictureBox1->Height;		//pictureboxの高さ
			 double scale = 5.0;							//表示倍率5→1/5で表示
			 int radius_of_wheels_for_render = int(radius_of_wheel / (double)scale);
			 int length_between_wheels_for_render = int(length_between_wheels / (double)scale);

			 if ((param_of_link + now_point - 1)->angle_of_left == -1){
				 now_point--;
				 while ((param_of_link + now_point)->angle_of_left == -1){ now_point--; if (now_point <= 0){ break; } }
			 }
			 else{
				 now_point--;
				 while ((param_of_link + now_point)->angle_of_left != -1){ now_point--; if (now_point <= 0){ break; } }
				 now_point++;
			 }

			 myBuffer->Graphics->Clear(Color::White);
			 int j = now_point;
			 int lc_r = int(length_between_wheels_for_render * cos((param_of_link + j)->angle_of_right));
			 int ls_r = int(length_between_wheels_for_render * sin((param_of_link + j)->angle_of_right));
			 int lc_l = int(length_between_wheels_for_render * cos((param_of_link + j)->angle_of_left));
			 int ls_l = int(length_between_wheels_for_render * sin((param_of_link + j)->angle_of_left));
			 int center_of_link_x = int((road_shape + j)->distance / scale) % camx;
			 int step_x = int((road_shape + j)->distance / scale) / camx;
			 int center_of_link_r = int(10 + 300 * step_x + (param_of_link + j)->y_of_center_of_link_of_right_side / scale);
			 int center_of_link_l = int(10 + 300 * step_x + (param_of_link + j)->y_of_center_of_link_of_left_side / scale);
			 myBuffer->Graphics->DrawLine(blackPen, center_of_link_x - lc_r, center_of_link_r - ls_r, center_of_link_x + lc_r, center_of_link_r + ls_r);
			 myBuffer->Graphics->FillEllipse(redBrush, center_of_link_x - lc_r - radius_of_wheels_for_render, center_of_link_r - ls_r - radius_of_wheels_for_render, radius_of_wheels_for_render * 2, radius_of_wheels_for_render * 2);
			 myBuffer->Graphics->FillEllipse(redBrush, center_of_link_x + lc_r - radius_of_wheels_for_render, center_of_link_r + ls_r - radius_of_wheels_for_render, radius_of_wheels_for_render * 2, radius_of_wheels_for_render * 2);
			 myBuffer->Graphics->DrawLine(blackPen, center_of_link_x - lc_l, center_of_link_l - ls_l, center_of_link_x + lc_l, center_of_link_l + ls_l);
			 myBuffer->Graphics->FillEllipse(greenBrush, center_of_link_x - lc_l - radius_of_wheels_for_render, center_of_link_l - ls_l - radius_of_wheels_for_render, radius_of_wheels_for_render * 2, radius_of_wheels_for_render * 2);
			 myBuffer->Graphics->FillEllipse(greenBrush, center_of_link_x + lc_l - radius_of_wheels_for_render, center_of_link_l + ls_l - radius_of_wheels_for_render, radius_of_wheels_for_render * 2, radius_of_wheels_for_render * 2);

			 for (int i = 0; i < num_of_param; i++){
				 int road_x = int((road_shape + i)->distance / scale) % camx;
				 int step = int((road_shape + i)->distance / scale) / camx;
				 int road_hl = int((road_shape + i)->height_of_left / scale);
				 int road_hr = int((road_shape + i)->height_of_right / scale);
				 myBuffer->Graphics->FillEllipse(greenBrush, road_x, 10 + road_hl + 300 * step, 2, 2);
				 myBuffer->Graphics->FillEllipse(redBrush, road_x, 10 + road_hr + 300 * step, 2, 2);
			 }
			 myBuffer->Render(gr);
}
};
}